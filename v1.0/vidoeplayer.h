#pragma once

#include <QThread>
#include <QImage>
#include <QtConcurrent/qtconcurrentrun.h>

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/pixfmt.h"
#include "libswscale/swscale.h"
}
extern "C"
{
#pragma comment (lib, "Ws2_32.lib")    
#pragma comment (lib, "avcodec.lib")  
#pragma comment (lib, "avdevice.lib")  
#pragma comment (lib, "avfilter.lib")  
#pragma comment (lib, "avformat.lib")  
#pragma comment (lib, "avutil.lib")  
#pragma comment (lib, "swresample.lib")  
#pragma comment (lib, "swscale.lib")  
};

class vidoeplayer : public QThread
{
	Q_OBJECT

public:
	vidoeplayer();
	~vidoeplayer();
	void startPlay();
	//void writeFrame();

	AVFormatContext *pFormatCtx;
	AVCodecContext *pCodecCtx;
	AVCodec *pCodec;
	AVFrame *pFrame, *pFrameRGB;
	AVFrame *pFrameYUV;
	AVPacket *packet;
	uint8_t *out_buffer;
	uint8_t *out_buffer_YUV;
	struct SwsContext *img_convert_yuv;

signals:
	void sig_GetOneFrame(QImage); //每获取到一帧图像 就发送此信号
signals:
	void sig_GetRFrame(QImage);

protected:
	void run();

private:
	QString mFileName;
};
