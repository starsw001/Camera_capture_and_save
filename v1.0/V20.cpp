#include "V20.h"
#include <iostream>
using namespace std;

V20::V20(QWidget *parent)
	: QMainWindow(parent)
	,ui(new Ui::V10Class)
{
	ui->setupUi(this);
	connect(ui->Convert_Start, SIGNAL(clicked()), SLOT(start_cv()));
}

V20::~V20()
{
	delete ui;
}
void V20::start_cv()
{
	cv.input_w = ui->width_lineEdit->text().toInt();
	cv.input_h = ui->height_lineEdit->text().toInt();
	cv.framenumber = ui->fps_lineEdit->text().toInt();
	//QByteArray ba = ui->outfilename_lineEdit->text().toLatin1();
	//cv.out_file_name = ba.data();
	//cout << cv.out_file_name << endl;
	cv.StartConvert();
}